# Dunelm Data Engineer Tech Test

Please clone this repository and test that example.py can run successfully before your interview.

The test itself will be done as pair programming session during your interview, and we don't expect you to have any code prepared beforehand.

During the interview all tools are allowed (Google etc).


## Prerequisites

* Python 3


