from typing import List


def json_flatten(json_file_path: str, keys: List[str], output_file_path: str) -> bool:
    """

    :param json_file_path: File path to read nested JSON from (e.g. product_example.json)
    :param keys: A list of json key paths to be flattened
    :param output_file_path: File path to write flattened JSON to (e.g. example_output.json)
    :return:
    """
    print("Run successfully")


json_flatten(
    "JSON/example_input.json", ["key1.key2", "key3.key4"], "JSON/example_output_from_script.json"
)
